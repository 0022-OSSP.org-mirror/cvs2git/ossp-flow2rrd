   _        ___  ____ ____  ____    __ _               ____               _
  |_|_ _   / _ \/ ___/ ___||  _ \  / _| | _____      _|___ \ _ __ _ __ __| |
  _|_||_| | | | \___ \___ \| |_) || |_| |/ _ \ \ /\ / / __) | '__| '__/ _` |
 |_||_|_| | |_| |___) |__) |  __/ |  _| | (_) \ V  V / / __/| |  | | | (_| |
  |_|_|_|  \___/|____/____/|_|    |_| |_|\___/ \_/\_/ |_____|_|  |_|  \__,_|

  OSSP flow2rrd - NetFlow to Round-Robin Database

  ChangeLog

  Changes between 0.9.1 and 0.9.2 (26-Dec-2004 to 27-Dec-2004):

    *) Calculate a more reasonable Y axis auto-scaling based
       on average numbers instead of maximum numbers.
       [Ralf S. Engelschall]

    *) Better formatting of legend by manually aligning elements
       in width-dependent number of columns and by prefixing elements
       with calculated average traffic.
       [Ralf S. Engelschall]

    *) Add --verbose option and use it to show a flow/sec indication
       under option --store.
       [Ralf S. Engelschall]

    *) Make RRD internal setup configurable in flow2rrd.cfg with two
       sub-directives (Stepping and Storage) of the Database directive.
       [Ralf S. Engelschall]
    
    *) Reduce flow accumulation window from 5 min to 1 min in order
       to get more detailed graphs.
       [Ralf S. Engelschall]

    *) Speed up processing of flows under "flow2rrd --stable" a little bit.
       [Ralf S. Engelschall]

    *) Allow floating point numbers for the time ranges in the Web UI.
       [Ralf S. Engelschall]

    *) Reduce color-reduction for incoming traffic to not underflow too
       fast and render the incoming/outgoing legend more as a table.
       Also, the MGRIDs are now drawn less black and the day borders are
       drawn fully black.
       [Ralf S. Engelschall]

    *) Remove out-commented code.
       [Ralf S. Engelschall]

    *) Match flows against targets in the same order the targets
       occur in the configuration file to allow overlapping
       networks (where the first matching one is taken).
       [Ralf S. Engelschall, Christoph Schug]

    *) Small source tree cleanups.
       [Ralf S. Engelschall]

    *) Implement the wildcard port handling.
       [Ralf S. Engelschall]

  Changes between 0.9.0 and 0.9.1 (26-Dec-2004 to 26-Dec-2004):

    *) Adjust default configuration path.
       [Ralf S. Engelschall]

    *) Create "localstatedir" under "make install" and
       place default RRD into this directory.
       [Ralf S. Engelschall]

    *) Fix syntax of default configuration.
       [Ralf S. Engelschall]

  Changes between *GENESIS* and 0.9.0 (18-Dec-2004 to 26-Dec-2004):

    *) Created the initial version of OSSP flow2rrd.
       [Ralf S. Engelschall]

