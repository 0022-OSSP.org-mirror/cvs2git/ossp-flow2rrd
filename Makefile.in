##
##  OSSP flow2rrd -- NetFlow to Round-Robin Database
##  Copyright (c) 2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP flow2rrd, a tool for storing NetFlow data
##  into an RRD which can be found at http://www.ossp.org/pkg/tool/flow2rrd/.
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 2 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  Makefile.in: make(1) build procedure
##

@SET_MAKE@

#   installation paths
DESTDIR       =
prefix        = @prefix@
exec_prefix   = @exec_prefix@
bindir        = @bindir@
sysconfdir    = @sysconfdir@
libdir        = @libdir@
includedir    = @includedir@
mandir        = @mandir@
localstatedir = @localstatedir@

#   build tools
RM            = rm -f
RMDIR         = rmdir
SHTOOL        = ./shtool
TRUE          = true
PERL          = @PERL@
POD2MAN       = @POD2MAN@

#   default targets
all: flow2rrd flow2rrd.1

#   generate program
flow2rrd: flow2rrd.pl
	@echo "generating: flow2rrd <-- flow2rrd.pl"; \
	V=`$(SHTOOL) version -l txt -d long VERSION`; \
	sed -e "s;@PERL\@;$(PERL);g" \
	    -e "s;@POD2MAN\@;$(POD2MAN);g" \
	    -e "s;@SYSCONFDIR\@;$(sysconfdir);g" \
	    -e "s;@VERSION\@;$$V;g" \
	    <flow2rrd.pl >flow2rrd && chmod a+x flow2rrd

#   generate manual page
flow2rrd.1: flow2rrd.pod
	@echo "generating: flow2rrd.1 <-- flow2rrd.pod"; \
	V1=`$(SHTOOL) version -l txt -d short VERSION`; \
	V2=`$(SHTOOL) version -l txt -d long VERSION`; \
	D=`$(SHTOOL) version -l txt -d long VERSION | sed -e 's;.*(;;' -e 's;).*;;'`; \
	$(POD2MAN) --quotes=none \
	           --section=1 --center="NetFlow to Round-Robin Database (RRD)" \
	           --release="$$D" --date="OSSP flow2rrd $$V1" flow2rrd.pod | \
	sed -e "s;FLOW2RRD_VERSION_STR;$$V2;" >flow2rrd.1

#   perform installation procedure
install: all
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(prefix)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(bindir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(mandir)/man1
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(sysconfdir)
	$(SHTOOL) mkdir -f -p -m 755 $(DESTDIR)$(localstatedir)
	$(SHTOOL) install -c -m 755 flow2rrd $(DESTDIR)$(bindir)/
	$(SHTOOL) install -c -m 644 flow2rrd.1 $(DESTDIR)$(mandir)/man1/
	$(SHTOOL) install -c -m 644 -e 's;@LOCALSTATEDIR\@;$(localstatedir);g' flow2rrd.cfg $(DESTDIR)$(sysconfdir)/

#   perform uninstallation procedure
uninstall:
	-$(RM) $(DESTDIR)$(bindir)/flow2rrd
	-$(RM) $(DESTDIR)$(mandir)/man1/flow2rrd.1
	-$(RM) $(DESTDIR)$(sysconfdir)/flow2rrd.cfg
	-$(RMDIR) $(DESTDIR)$(bindir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(mandir)/man1 >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(mandir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(sysconfdir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(localstatedir) >/dev/null 2>&1 || $(TRUE)
	-$(RMDIR) $(DESTDIR)$(prefix) >/dev/null 2>&1 || $(TRUE)

#   perform build clean up procedure
clean:
	-$(RM) flow2rrd
	-$(RM) flow2rrd.1

#   perform distribution clean up procedure
distclean: clean
	-$(RM) config.log config.status config.cache
	-$(RM) Makefile

#   perform developer clean up procedure
realclean: distclean

